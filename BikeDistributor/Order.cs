﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace BikeDistributor
{
    public class Order
    {
        public IList<Line> Lines { get; private set; }
        public string Company { get; private set; }
        public static double TaxRate { get; private set; }
        public CultureInfo Culture { get; private set; }

        public IAmountEvaluator AmountEvaluator { get; private set; }

        public Order(IOrderConfiguration configuration)
        {
            Company = configuration.Company;
            Culture = configuration.CultureInfo;
            TaxRate = configuration.TaxRate;
            AmountEvaluator = configuration.AmountEvaluator;
            Lines = new List<Line>();
        }

        public void AddLine(Line line)
        {
            Lines.Add(line);
        }

        public Receipt Receipt()
        {
            return new ReceiptBuilder(this).Build();
        }

        public string StringReceipt()
        {
            return new ReceiptStringPrinter(new ReceiptBuilder(this).Build()).Print();
        }

        public string HtmlReceipt()
        {
            return new ReceiptHtmlPrinter(new ReceiptBuilder(this).Build()).Print();
        }
    }
}
