﻿using System.Globalization;

namespace BikeDistributor
{
    public class OrderConfiguration : IOrderConfiguration
    {
        public OrderConfiguration()
        {
            CultureInfo = CultureInfo.GetCultureInfo("en-US");
            TaxRate = .0725d;
            AmountEvaluator = new StandardAmountEvaluator();
        }

        public OrderConfiguration(string company):this()
        {
            Company = company; 
        }

        public string Company { get; set; }
        public CultureInfo CultureInfo { get; set; }
        public double TaxRate { get; set; }
        public IAmountEvaluator AmountEvaluator { get; set; }
    }
}
