﻿using System.Globalization;

namespace BikeDistributor
{
    public interface IOrderConfiguration
    {
        IAmountEvaluator AmountEvaluator { get; set; }
        string Company { get; set; }
        CultureInfo CultureInfo { get; set; }
        double TaxRate { get; set; }
    }
}