﻿namespace BikeDistributor
{
    public class Bike
    {
        public const double OneThousand = 1000d;
        public const double TwoThousand = 2000d;
        public const double FiveThousand = 5000d;
    
        public Bike(string brand, string model, double price)
        {
            Brand = brand;
            Model = model;
            Price = price;
        }

        public string Brand { get; private set; }
        public string Model { get; private set; }
        public double Price { get; set; }
    }
}
