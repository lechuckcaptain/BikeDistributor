﻿namespace BikeDistributor
{
    public class LineAmount : Line
    {
        public double Amount { get; private set; }

        public LineAmount(Line line, IAmountEvaluator evaluator) : base(line.Bike, line.Quantity)
        {
            Amount = evaluator.Evaluate(line);
        }
    }
}