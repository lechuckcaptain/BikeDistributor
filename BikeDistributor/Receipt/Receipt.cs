﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeDistributor
{
    public class Receipt
    {
        public double Total { get; private set; }
        public double Tax { get; private set; }
        public double Subtotal { get; private set; }

        public List<LineAmount> LinesAmounts { get; private set; }

        public string Company { get; private set; }
        public CultureInfo Culture { get; private set; }

        public Receipt(CultureInfo culture, string company, double total, double tax, double subtotal, List<LineAmount> lines)
        {
            this.Culture = culture;
            this.Company = company;
            this.Total = total;
            this.Tax = tax;
            this.Subtotal = subtotal;
            this.LinesAmounts = lines;
        }
    }
}
