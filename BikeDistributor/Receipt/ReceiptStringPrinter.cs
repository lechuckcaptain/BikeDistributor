﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeDistributor
{
    public class ReceiptStringPrinter
    {
        private Receipt _receipt;

        public ReceiptStringPrinter(Receipt receipt)
        {
            _receipt = receipt;
        }

        public string Print()
        {
            var result = new StringBuilder(string.Format(_receipt.Culture, "Order Receipt for {0}{1}", _receipt.Company, Environment.NewLine));
            foreach (var lineAmount in _receipt.LinesAmounts)
            {
                result.AppendLine(string.Format(_receipt.Culture, "\t{0} x {1} {2} = {3:C}", lineAmount.Quantity, lineAmount.Bike.Brand, lineAmount.Bike.Model, lineAmount.Amount));
            }
            result.AppendLine(string.Format(_receipt.Culture, "Sub-Total: {0:C}", _receipt.Subtotal));
            result.AppendLine(string.Format(_receipt.Culture, "Tax: {0:C}", _receipt.Tax));
            result.Append(string.Format(_receipt.Culture, "Total: {0:C}", _receipt.Total));
            return result.ToString();
        }
    }
}
