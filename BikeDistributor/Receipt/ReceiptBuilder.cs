﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeDistributor
{
    public class ReceiptBuilder
    {
        private Order _order;

        public ReceiptBuilder(Order order)
        {
            _order = order;
        }

        internal Receipt Build()
        {
            var totalAmount = 0d;
            var lines = new List<LineAmount>();

            foreach (var line in _order.Lines)
            {
                var lineAmount = new LineAmount(line, _order.AmountEvaluator);
                lines.Add(lineAmount);
                totalAmount += lineAmount.Amount;
            }

            var subtotal = totalAmount;
            var tax = totalAmount * Order.TaxRate;
            var total = totalAmount + tax;

            return new Receipt(_order.Culture, _order.Company, total, tax, subtotal, lines);
        }
    }
}
