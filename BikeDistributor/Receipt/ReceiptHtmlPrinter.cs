﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeDistributor
{
    public class ReceiptHtmlPrinter
    {
        private Receipt _receipt;

        public ReceiptHtmlPrinter(Receipt receipt)
        {
            _receipt = receipt;
        }

        public string Print()
        {
            var result = new StringBuilder(string.Format(_receipt.Culture, "<html><body><h1>Order Receipt for {0}</h1>", _receipt.Company));
            if (_receipt.LinesAmounts.Any())
            {
                result.Append("<ul>");
                foreach (var lineAmount in _receipt.LinesAmounts)
                {
                    result.Append(string.Format(_receipt.Culture, "<li>{0} x {1} {2} = {3:C}</li>", lineAmount.Quantity, lineAmount.Bike.Brand, lineAmount.Bike.Model, lineAmount.Amount));
                }
                result.Append("</ul>");
            }
            result.Append(string.Format(_receipt.Culture, "<h3>Sub-Total: {0:C}</h3>", _receipt.Subtotal));
            result.Append(string.Format(_receipt.Culture, "<h3>Tax: {0:C}</h3>", _receipt.Tax));
            result.Append(string.Format(_receipt.Culture, "<h2>Total: {0:C}</h2>", _receipt.Total));
            result.Append("</body></html>");
            return result.ToString();
        }
    }
}
