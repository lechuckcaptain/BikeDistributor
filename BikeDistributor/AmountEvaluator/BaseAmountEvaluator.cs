﻿namespace BikeDistributor
{
    public class BaseAmountEvaluator: IAmountEvaluator
    {
        public double Evaluate(Line line)
        {
            return line.Quantity * line.Bike.Price;
        }
    }
}