﻿namespace BikeDistributor
{
    public class StandardAmountEvaluator:IAmountEvaluator
    {
        public double Evaluate(Line line)
        {
            if (line.Bike.Price <= Bike.OneThousand)
            {
                return EvaluateDiscount(line, 20, .9d);
            }
            else if (line.Bike.Price <= Bike.TwoThousand)
            {
                return EvaluateDiscount(line, 10, .8d);
            }
            else if (line.Bike.Price <= Bike.FiveThousand)
            {
                return EvaluateDiscount(line, 5, .8d);
            }
            else
            {
                // TODO: Define new ranges
                return EvaluateDiscount(line, 0, 1);
            }
        }

        private static double EvaluateDiscount(Line line, int quantityForDiscount, double discountPercentage)
        {
            if (line.Quantity >= quantityForDiscount)
                return line.Quantity * line.Bike.Price * discountPercentage;
            else
                return line.Quantity * line.Bike.Price;
        }
    }

 

}