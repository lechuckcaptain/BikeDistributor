﻿namespace BikeDistributor
{
    public interface IAmountEvaluator
    {
        double Evaluate(Line line);
    }
}