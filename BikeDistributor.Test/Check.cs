﻿namespace BikeDistributor.Test
{
    public class Check
    {
        public static readonly Check Company = new Check("Company", CheckType.String);
        public static readonly Check Tax = new Check("Tax", CheckType.Currency);
        public static readonly Check SubTotal = new Check("SubTotal", CheckType.Currency);
        public static readonly Check Total = new Check("Total", CheckType.Currency);
        public static readonly Check NumLines = new Check("Num Lines", CheckType.Int);
        public static readonly Check Brand = new Check("Brand", CheckType.String);
        public static readonly Check Model = new Check("Model", CheckType.String);
        public static readonly Check Price = new Check("Price", CheckType.Currency);
        public static readonly Check Amount = new Check("Amount", CheckType.Currency);


        public Check(string desc, CheckType type)
        {
            Desc = desc;
            Type = type;
        }

        public string Desc { get; private set; }
        public CheckType Type { get; private set; }
    }

}


