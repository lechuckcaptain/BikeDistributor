﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeDistributor.Test
{
    [TestClass]
    public class BaseTest
    {
        private const String SEP = "#######################################################################################################################################################";
        public TestContext TestContext { get; set; }

        protected readonly static String ANYWHERE_COMPANY = "Anywhere Bike Shop";

        protected readonly static Bike Defy = new Bike("Giant", "Defy 1", Bike.OneThousand);
        protected readonly static Bike Elite = new Bike("Specialized", "Venge Elite", Bike.TwoThousand);
        protected readonly static Bike DuraAce = new Bike("Specialized", "S-Works Venge Dura-Ace", Bike.FiveThousand);


        protected void AreEqual(object expected, object actual, Check check)
        {
            //TODO: Improve here 
            switch (check.Type)
            {
                case CheckType.Int:
                    IntEqual((int)expected, (int)actual, check.Desc);
                    break;
                case CheckType.Currency:
                    CurrencyEqual((double)expected, (double)actual, check.Desc);
                    break;
                case CheckType.String:
                    StringEqual((string)expected, (string)actual, check.Desc);
                    break;
                case CheckType.Text:
                    TextEqual((string)expected, (string)actual);
                    break;
            }
        }

        protected void TextEqual(string expected, string actual)
        {
            StringEqual(expected.RemoveWhiteSpaces(), actual.RemoveWhiteSpaces(), "No white spaces");
            StringEqual(expected.RemoveTabs(), actual.RemoveTabs(), "No tabs");
            StringEqual(expected.RemoveNewLines(), actual.RemoveNewLines(), "No new lines");
            StringEqual(expected.ReplaceWhiteSpaces(), actual.ReplaceWhiteSpaces(), "Replace white spaces");
            StringEqual(expected, actual);
        }

        protected void StringEqual(string expected, string actual, string desc = null)
        {
            AddToContext(expected, actual, desc);
            Assert.AreEqual(expected, actual);
        }

        protected void CurrencyEqual(double expected, double actual, string desc = null)
        {
            AddToContext(expected.ToString("C"), actual.ToString("C"), desc);
            Assert.AreEqual(expected, actual, 0.001);
        }

        protected void IntEqual(int expected, int actual, string desc = null)
        {
            AddToContext(expected.ToString(), actual.ToString(), desc);
            Assert.AreEqual(expected, actual);
        }

        private void AddToContext(string expected, string actual, string desc)
        {
            TestContext.WriteLine(SEP);

            if (desc != null) TestContext.WriteLine("Check equal: '{0}'", desc);

            TestContext.WriteLine("EXP: '{0}'", expected);
            TestContext.WriteLine("ACT: '{0}'", actual);
        }

        [TestInitialize]
        public void Init()
        {
            TestInitialize();
        }

        protected virtual void TestInitialize()
        {
            TestContext.WriteLine(SEP);
            TestContext.WriteLine("OS: {0}", Environment.OSVersion);
            TestContext.WriteLine("MachineName: {0}", Environment.MachineName);
            TestContext.WriteLine("Version: {0}", Environment.Version);
        }
    }
}
