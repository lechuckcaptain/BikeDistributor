﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace BikeDistributor.Test
{
    [TestClass]
    public class ContentTest: BaseTest
    {
        [TestMethod]
        public void ReceiptOneDefy()
        {
            var order = new Order(new OrderConfiguration(ANYWHERE_COMPANY));
            order.AddLine(new Line(Defy, 1));
            var receipt = order.Receipt();

            AreEqual(ANYWHERE_COMPANY, receipt.Company, Check.Company);
            AreEqual(72.50d, receipt.Tax, Check.Tax);
            AreEqual(1000d, receipt.Subtotal, Check.SubTotal);
            AreEqual(1072.50d, receipt.Total, Check.Total);
            AreEqual(1, receipt.LinesAmounts.Count, Check.NumLines);
            AreEqual(Defy.Brand, receipt.LinesAmounts[0].Bike.Brand, Check.Brand);
            AreEqual(Defy.Model, receipt.LinesAmounts[0].Bike.Model, Check.Model);
            AreEqual(Defy.Price, receipt.LinesAmounts[0].Bike.Price, Check.Price);
            AreEqual((double) Defy.Price, receipt.LinesAmounts[0].Amount, Check.Amount);
        }

        [TestMethod]
        public void ReceiptThreeDefy()
        {
            var order = new Order(new OrderConfiguration(ANYWHERE_COMPANY));
            order.AddLine(new Line(Defy, 3));
            var receipt = order.Receipt();

            AreEqual(ANYWHERE_COMPANY, receipt.Company, Check.Company);
            AreEqual(217.50d, receipt.Tax, Check.Tax);
            AreEqual(3000d, receipt.Subtotal, Check.SubTotal);
            AreEqual(3217.50d, receipt.Total, Check.Total);
            AreEqual(1, receipt.LinesAmounts.Count, Check.NumLines);
            AreEqual(Defy.Brand, receipt.LinesAmounts[0].Bike.Brand, Check.Brand);
            AreEqual(Defy.Model, receipt.LinesAmounts[0].Bike.Model, Check.Model);
            AreEqual(Defy.Price, receipt.LinesAmounts[0].Bike.Price, Check.Price);
            AreEqual((double) (Defy.Price * 3), receipt.LinesAmounts[0].Amount, Check.Amount);
        }

        [TestMethod]
        public void ReceiptThreeDefyHighTaxRate()
        {
            var order = new Order(new OrderConfiguration(ANYWHERE_COMPANY) { TaxRate = 0.5d });
            order.AddLine(new Line(Defy, 3));
            var receipt = order.Receipt();

            AreEqual(ANYWHERE_COMPANY, receipt.Company, Check.Company);
            AreEqual(1500d, receipt.Tax, Check.Tax);
            AreEqual(3000d, receipt.Subtotal, Check.SubTotal);
            AreEqual(4500d, receipt.Total, Check.Total);
            AreEqual(1, receipt.LinesAmounts.Count, Check.NumLines);
            AreEqual(Defy.Brand, receipt.LinesAmounts[0].Bike.Brand, Check.Brand);
            AreEqual(Defy.Model, receipt.LinesAmounts[0].Bike.Model, Check.Model);
            AreEqual(Defy.Price, receipt.LinesAmounts[0].Bike.Price, Check.Price);
            AreEqual((double)(Defy.Price * 3), receipt.LinesAmounts[0].Amount, Check.Amount);
        }

        [TestMethod]
        public void ReceiptFiftyDefy()
        {
            var order = new Order(new OrderConfiguration(ANYWHERE_COMPANY));
            order.AddLine(new Line(Defy, 50));
            var receipt = order.Receipt();

            AreEqual(ANYWHERE_COMPANY, receipt.Company, Check.Company);
            AreEqual(3262.5d, receipt.Tax, Check.Tax);
            AreEqual(45000d, receipt.Subtotal, Check.SubTotal);
            AreEqual(48262.5d, receipt.Total, Check.Total);
            AreEqual(1, receipt.LinesAmounts.Count, Check.NumLines);
            AreEqual(Defy.Brand, receipt.LinesAmounts[0].Bike.Brand, Check.Brand);
            AreEqual(Defy.Model, receipt.LinesAmounts[0].Bike.Model, Check.Model);
            AreEqual(Defy.Price, receipt.LinesAmounts[0].Bike.Price, Check.Price);
            AreEqual((double)(Defy.Price * 50) * .9d, receipt.LinesAmounts[0].Amount, Check.Amount);
        }

        [TestMethod]
        public void ReceiptFiftyDefyBaseAmountEvaluator()
        {
            var order = new Order(new OrderConfiguration() { Company = ANYWHERE_COMPANY, AmountEvaluator = new BaseAmountEvaluator() });
            order.AddLine(new Line(Defy, 50));
            var receipt = order.Receipt();

            AreEqual(ANYWHERE_COMPANY, receipt.Company, Check.Company);
            AreEqual(3625d, receipt.Tax, Check.Tax);
            AreEqual(50000d, receipt.Subtotal, Check.SubTotal);
            AreEqual(53625d, receipt.Total, Check.Total);
            AreEqual(1, receipt.LinesAmounts.Count, Check.NumLines);
            AreEqual(Defy.Brand, receipt.LinesAmounts[0].Bike.Brand, Check.Brand);
            AreEqual(Defy.Model, receipt.LinesAmounts[0].Bike.Model, Check.Model);
            AreEqual(Defy.Price, receipt.LinesAmounts[0].Bike.Price, Check.Price);
            AreEqual((double)(Defy.Price * 50), receipt.LinesAmounts[0].Amount, Check.Amount);
        }

        [TestMethod]
        public void ReceiptNewBike()
        {
            Bike newBike = new Bike("New cool brand bike", "Super Giga 0", 3549.75d);

            var order = new Order(new OrderConfiguration(ANYWHERE_COMPANY));
            order.AddLine(new Line(newBike, 50));
            var receipt = order.Receipt();

            AreEqual(ANYWHERE_COMPANY, receipt.Company, Check.Company);
            AreEqual(10294.275d, receipt.Tax, Check.Tax);
            AreEqual(141990d, receipt.Subtotal, Check.SubTotal);
            AreEqual(152284.275, receipt.Total, Check.Total);
            AreEqual(1, receipt.LinesAmounts.Count, Check.NumLines);
            AreEqual(newBike.Brand, receipt.LinesAmounts[0].Bike.Brand, Check.Brand);
            AreEqual(newBike.Model, receipt.LinesAmounts[0].Bike.Model, Check.Model);
            AreEqual(newBike.Price, receipt.LinesAmounts[0].Bike.Price, Check.Price);
            AreEqual((double)(newBike.Price * 50) * .8d, receipt.LinesAmounts[0].Amount, Check.Amount);
        }
    }
}


