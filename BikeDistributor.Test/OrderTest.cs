﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace BikeDistributor.Test
{
    [TestClass]
    public class OrderTest: BaseTest
    {
        private readonly static Bike Defy = new Bike("Giant", "Defy 1", Bike.OneThousand);
        private readonly static Bike Elite = new Bike("Specialized", "Venge Elite", Bike.TwoThousand);
        private readonly static Bike DuraAce = new Bike("Specialized", "S-Works Venge Dura-Ace", Bike.FiveThousand);

        private string ResultStatementOneDefy;
        private string ResultStatementOneElite;
        private string ResultStatementOneDuraAce;
        private string ResultStatementOneDefyParam;
        private string HtmlResultStatementOneDefy;
        private string HtmlResultStatementOneElite;
        private string HtmlResultStatementOneDuraAce;

        protected override void TestInitialize()
        {
            base.TestInitialize();
        
            ResultStatementOneDefy = System.IO.File.ReadAllText(@"TestFiles\ResultStatementOneDefy.txt");
            ResultStatementOneElite = System.IO.File.ReadAllText(@"TestFiles\ResultStatementOneElite.txt");
            ResultStatementOneDuraAce = System.IO.File.ReadAllText(@"TestFiles\ResultStatementOneDuraAce.txt");

            ResultStatementOneDefyParam = System.IO.File.ReadAllText(@"TestFiles\ResultStatementOneDefyParam.txt");

            HtmlResultStatementOneDefy = @"<html><body><h1>Order Receipt for Anywhere Bike Shop</h1><ul><li>1 x Giant Defy 1 = $1,000.00</li></ul><h3>Sub-Total: $1,000.00</h3><h3>Tax: $72.50</h3><h2>Total: $1,072.50</h2></body></html>";
            HtmlResultStatementOneElite = @"<html><body><h1>Order Receipt for Anywhere Bike Shop</h1><ul><li>1 x Specialized Venge Elite = $2,000.00</li></ul><h3>Sub-Total: $2,000.00</h3><h3>Tax: $145.00</h3><h2>Total: $2,145.00</h2></body></html>";
            HtmlResultStatementOneDuraAce = @"<html><body><h1>Order Receipt for Anywhere Bike Shop</h1><ul><li>1 x Specialized S-Works Venge Dura-Ace = $5,000.00</li></ul><h3>Sub-Total: $5,000.00</h3><h3>Tax: $362.50</h3><h2>Total: $5,362.50</h2></body></html>";
        }

        [TestMethod]
        public void StringReceiptOneDefy()
        {
            var order = new Order(new OrderConfiguration("Anywhere Bike Shop"));
            order.AddLine(new Line(Defy, 1));
            TextEqual(ResultStatementOneDefy, order.StringReceipt());
        }

        [TestMethod]
        public void StringOneDefyEuro()
        {
            var it_culture = CultureInfo.GetCultureInfo("it-IT");
            var order = new Order(new OrderConfiguration(ANYWHERE_COMPANY) { CultureInfo = it_culture });
            order.AddLine(new Line(Defy, 1));
            var receipt = order.Receipt();
            TextEqual(string.Format(it_culture, ResultStatementOneDefyParam, 1, 1000, 1000, 72.5, 1072.5), order.StringReceipt());
        }

        [TestMethod]
        public void StringThreeDefyEuro()
        {
            var it_culture = CultureInfo.GetCultureInfo("it-IT");
            var order = new Order(new OrderConfiguration(ANYWHERE_COMPANY) { CultureInfo = it_culture });
            order.AddLine(new Line(Defy, 3));
            var receipt = order.Receipt();
            TextEqual(string.Format(it_culture, ResultStatementOneDefyParam, 3, 3000, 3000, 217.5, 3217.5), order.StringReceipt());
        }

        [TestMethod]
        public void StringReceiptOneElite()
        {
            var order = new Order(new OrderConfiguration(ANYWHERE_COMPANY));
            order.AddLine(new Line(Elite, 1));
            TextEqual(ResultStatementOneElite, order.StringReceipt());
        }

        [TestMethod]
        public void StringReceiptOneDuraAce()
        {
            var order = new Order(new OrderConfiguration(ANYWHERE_COMPANY));
            order.AddLine(new Line(DuraAce, 1));
            TextEqual(ResultStatementOneDuraAce, order.StringReceipt());
        }

        [TestMethod]
        public void HtmlReceiptOneDefy()
        {
            var order = new Order(new OrderConfiguration(ANYWHERE_COMPANY));
            order.AddLine(new Line(Defy, 1));
            TextEqual(HtmlResultStatementOneDefy, order.HtmlReceipt());
        }

        [TestMethod]
        public void HtmlReceiptOneElite()
        {
            var order = new Order(new OrderConfiguration(ANYWHERE_COMPANY));
            order.AddLine(new Line(Elite, 1));
            TextEqual(HtmlResultStatementOneElite, order.HtmlReceipt());
        }

        [TestMethod]
        public void HtmlReceiptOneDuraAce()
        {
            var order = new Order(new OrderConfiguration(ANYWHERE_COMPANY));
            order.AddLine(new Line(DuraAce, 1));
            TextEqual(HtmlResultStatementOneDuraAce, order.HtmlReceipt());
        }
    }
}


