﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BikeDistributor.Test
{
    public static class StringExtensions
    {
        private readonly static Regex WHITE_SPACES_REGEX = new Regex(@"\s");
        private readonly static Regex TABS_REGEX = new Regex(@"\t");
        private readonly static Regex NL_REGEX = new Regex(@"\n");
        private readonly static Regex CR_REGEX = new Regex(@"\r");

        public static string RemoveWhiteSpaces(this String str)
        {
            return WHITE_SPACES_REGEX.Replace(str,String.Empty);
        }

        public static string RemoveTabs(this String str)
        {
            return TABS_REGEX.Replace(str, String.Empty);
        }

        public static string RemoveNewLines(this String str)
        {
            return CR_REGEX.Replace(NL_REGEX.Replace(str, String.Empty),String.Empty);
        }

        public static string ReplaceWhiteSpaces(this String str)
        {
            return WHITE_SPACES_REGEX.Replace(TABS_REGEX.Replace(CR_REGEX.Replace(NL_REGEX.Replace(str, "<LF>"),"<CR>"),"<TB>"),"<WS>");
        }
    }
}
